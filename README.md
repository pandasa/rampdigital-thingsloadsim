# README #

RampDigital Node-Red Things Load Simluator Prototype 

### How do I get set up? ###

* Dependencies: NodeJS, Node-RED, Node-RED Dashboard
	* To install NodeJS: `brew install node` 
	* To install Node-RED: `sudo npm install -g --unsafe-perm node-red`
  	* To install Node-RED Dashboard: `cd ~/.node-red && npm install node-red-dashboard`

* Clone into ~/.node-red/lib/flows
	* `cd ~/.node-red/lib/flows && git clone https://pandasa@bitbucket.org/pandasa/rampdigital-thingsloadsim.git`

* Importing the flow
	* Start Node-RED with `node-red` in terminal
	* To use Node-RED, open the browser and enter `localhost:1880`
	* On the top right hamburger menu, look for the "Import" selection
	* If the flow was properly cloned from the repository earlier, we should be able to see "Import >> Library >> rampdigital >> RampThingsLoadSimulator"

* Running
	* Open another terminal window and ssh into the rampdigital server to run the Relayr Cloud Demo nodejs application
	* Return to Node-RED and click "Deploy"
	* Go to `localhost:1880/ui` in the browser to access the dashboard
	* Click run and observe the terminal window with ssh to confirm that the test is, in fact, running
	* Data updates every few seconds

### How does it work? ###

* Using the MQTT protocol, we are able to communicate with the gateway device that we are simulating this test on
* We subscribe to the gateway's "data" topic to listen for messages sent by the gateway
* Data flows through the MQTT node as a string object and gets parsed into a JSON for simple extractions via name
	* Data objects that are useable at this point:
		* Test Run Number [0]
		* Number of Messages Sent [1]
		* Number of Messages Received [2]
		* % Messages Received [3]
		* \# Messages Delivered / ms [4]
		* \# Publish Errors [5]
		* \# Subscribe Errors [6]
		* Latency [7]
* We extract the data from the JSON array and feed it into our Node-RED dashboard components
* **Each component is carefully located through width and height configurations so any mondifications need to be carefully considered**
* If the test is running, "Start New" will be disabled and the Test Status will display "Running" through Run-By-Exception nodes 
* If we want to stop a test, we click "Stop". To start it again, we must restart the Relayr Cloud Demo on the gateway

### Bugs ###
* Configurations still do not work since messages to the /config topic isn't registering on the gateway 
* Live System Data (CPU, Memory, Disk usage) is still not operational
* Time Elapsed has no data from the server
* Need to fix activity based interactions (disabling the "Start New" button properly while the test is running)
* % Disk Usage graph sometimes renders illegibly

### WIP 
* Proper distinction of new and old data 
* Start test / stop test dashboard semantics 
* Adding data download capabilities
* Adding data email capabilities
* Adding hotswappable MQTT Broker URL options
* Hopefully moving away from Node-RED dashboard elements and using template HTMLs to offer greater flexibility
* Transitioning from MongoDB to InfluxDB 
* Historical Data 
